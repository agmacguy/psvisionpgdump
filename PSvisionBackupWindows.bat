REM Printsmith Vision Backup Script
REM Kevin Spears
REM Alphagraphics 004
REM Updated 9/17/2014
REM This is tested against Printsmith Vision 2.2 but should continue to work with later versions.

REM creates the file name for the backup, using date and time to make it unique
set hr=%time:~0,2%
if "%hr:~0,1%" equ " " set hr=0%hr:~1,1%
set datestr=%date:~-4,4%%date:~-10,2%%date:~-7,2%_%hr%%time:~3,2%%time:~6,2%
set BACKUP_FILE=printsmith_%datestr%.backup

REM Gets password config from PGADMIN, change to reflect your install.
SET PGPASSFILE=d:\printsmith\pgpass.conf

REM Runs the PG_DUMP app from the Postgres bin, I have it running with compression.
"c:\Program Files (x86)\EFI\PrintSmith\Postgres\bin\pg_dump.exe" -Fc -U postgres -f %BACKUP_FILE%  -d printsmith_db

REM This copies to my archive server, comment it out if unneeded
robocopy d:\printsmith\ \\files0\archives\printsmith\