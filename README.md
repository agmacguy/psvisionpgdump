## Synopsis

This script will allow you to automate the Postgres backups for PrintSmith Vision on Windows & Mac.

## Future Features

* Delete old daily backups, keep a monthly backup for X months.
* Auto install for the windows script
* Push backups to Amazon S3

## Installation Windows

1. Create a directory for your backups and copy the PSvisionBackupWindows.bat to this directory.
2. Copy or create your pgpass.conf to this directory. 
3. Comment out the Robocopy line if you don't want to copy your backups to the network.
4. Create a scheduled task to run the batch file… going to automate this install at some point.

## Installation Mac
The Mac install is really easy, just double click on the PSvisionBackupMac file. Everything is automated and it will also run your first backup.

## Tests

This software has been tested on Windows 8.1 and OS X 10.9 for Printsmith 2.2 & 2.2.1

## Contributors

If you are interested in this project and want to help improve my scripts let me know. I welcome any help that can be given.

## License

This project is licenced under the MIT License. You are welcome to use it and change it however you want.